#!/bin/sh

########################################
checkAndInstallPythonPackage()
{
  IMPORTNAME=$1

  if ! python -c "import $IMPORTNAME" >/dev/null 2>&1; then
    while test $# -gt 1; do
      shift
      PACKAGEINSTALL=$1
      echo failed: $PYTHON -c "import $IMPORTNAME"
      $PACKAGEINSTALL && return 0
    done
    echo >&1  $PACKAGEINSTALL failed
    exit 1
  fi
}

########################################
if which virtualenv-3.9 >/dev/null 2>&1; then
  MYVIRTUALENV=virtualenv-3.9
elif which virtualenv-3.8 >/dev/null 2>&1; then
  MYVIRTUALENV=virtualenv-3.8
elif which virtualenv-3.7 >/dev/null 2>&1; then
  MYVIRTUALENV=virtualenv-3.7
elif which virtualenv-3.6 >/dev/null 2>&1; then
  MYVIRTUALENV=virtualenv-3.6
elif which pyenv-virtualenv >/dev/null 2>&1; then
  # brew has pyenv-virtualenv
  # and a bug, "pyenv-root" should be written as "pyenv root"
  PYENV_ROOT="$(pyenv root)"
  export PYENV_ROOT
  MYVIRTUALENV="pyenv virtualenv"
  if test -e /usr/local/opt/python@3.7/bin; then
    PATH=/usr/local/opt/python@3.7/bin:$PATH
    export PATH
  fi
elif type virtualenv >/dev/null 2>&1; then
  MYVIRTUALENV=virtualenv
fi

echo MYVIRTUALENV=$MYVIRTUALENV
export MYVIRTUALENV

if test -z "$MYVIRTUALENV"; then
  echo >&2 "can not find virtualenv"
  exit 1
fi

##############################################################################
if type $MYVIRTUALENV >/dev/null 2>&1; then
  if which python3.9 >/dev/null 2>&1; then
    PYTHON=python3.9
  elif which python3.8 >/dev/null 2>&1; then
    PYTHON=python3.8
  elif which python3.7 >/dev/null 2>&1; then
    PYTHON=python3.7
  elif which python36 >/dev/null 2>&1; then
    PYTHON=python36
  elif which python3.6 >/dev/null 2>&1; then
    PYTHON=python3.6
  elif which python3.5 >/dev/null 2>&1; then
    PYTHON=python3.5
  elif which python36 >/dev/null 2>&1; then
    PYTHON=python36
  elif which python3.4 >/dev/null 2>&1; then
    PYTHON=python3.4
    # need $ pip install "pytest<5"
    PYTEST="pytest<5"
  else
    echo >&2 "No pyton 3.7, 3.6, 36 or 3.4 found"
    exit 1
  fi
  VIRTUALENVDIR=venv$PYTHON
fi

echo VIRTUALENVDIR=$VIRTUALENVDIR
###################

if test -d $HOME/.pyenv/versions/$VIRTUALENVDIR/bin/ ; then
  VIRTUALENVDIR=$HOME/.pyenv/versions/$VIRTUALENVDIR
fi
if test -r $VIRTUALENVDIR/bin/activate; then
  .  $VIRTUALENVDIR/bin/activate
else
  $MYVIRTUALENV --python=$PYTHON $VIRTUALENVDIR || {
    echo >&2 $MYVIRTUALENV failed
    exit 1
  }
fi
if test -r $VIRTUALENVDIR/bin/activate; then
  .  $VIRTUALENVDIR/bin/activate
fi
checkAndInstallPythonPackage numpy      "pip install numpy"
checkAndInstallPythonPackage matplotlib "pip install matplotlib"

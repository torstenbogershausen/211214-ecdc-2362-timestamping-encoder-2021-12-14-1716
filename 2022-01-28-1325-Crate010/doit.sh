#!/bin/sh
FILEWITHOUTEXT=monXX #monRawMtrEnc

FALSE=0
export FALSE

# path for crate007
#PATHINHOME=MCAG_setupMotionDemo.191003-base-7.0.5/epics/modules/ethercatmc/test/pythonscripts
#PATHINHOME=MCAG_setupMotionDemo/epics/modules/ethercatmc/test/pythonscripts
PATHINHOME=~/shared_EEE/projects/epics/MCAG_setupMotionDemo.210920/epics/modules/ethercatmc/test/pythonscripts
#if ! test -s $FILEWITHOUTEXT.txt; then
#  #IOC for Crate 007
#  IPADDR=172.30.244.38
#  if ping -c 1 $IPADDR; then
#    rsync -v -v torstenbogershausen@$IPADDR:/home/torstenbogershausen/$PATHINHOME/$FILEWITHOUTEXT.txt ./$FILEWITHOUTEXT.txt
#  fi
#fi

#if ! test -s $FILEWITHOUTEXT.txt; then
	cp $PATHINHOME/$FILEWITHOUTEXT.txt .
	cp $PATHINHOME/monXXX.txt  .
#fi


OTHER_PV=StatusCode
for PVNAME in RawMtrVelo RawMtrStep RawEncStep VelAct; do
#for PVNAME in RawMtrStep; do
  INPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.txt
  OUTPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.png
  if egrep "${PVNAME}|${OTHER_PV}" $FILEWITHOUTEXT.txt >$INPUTFILENAME; then
    python plot.py $INPUTFILENAME $OUTPUTFILENAME
  fi
done

import datetime
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.ticker as mticker
import re
import sys


"""
"""

debug = True #False


def main(argv=None):
    global debug
    if not argv:
        argv = sys.argv
    argc = len(argv)
    argvidx = 1
    if argv[argvidx] == "--debug":
        debug = True
        argvidx += 1
    if debug:
        print("len(argv)=%u " % argc)
        print("argv=%s" % argv)
    output_file_name = None
    input_file_name = argv[argvidx]
    argvidx += 1
    if len(argv) > argvidx:
        output_file_name = argv[argvidx]
    argvidx += 1

    old_steps = None
    wrap_around_offset = 0
    fid = open(input_file_name)
    lines = fid.readlines()

    # Collect date into an X- and Y- array
    x = []
    y = []
    oldSeconds = None
    quran_x_time = None
    quran_yvalue = None

    RE_MATCH_IOC_PV_NOT_FOUND = re.compile(
        r".*( \*\*\* Not connected \(PV not found\))"
    )
    RE_MATCH_IOC_PV_DISCONNECTED = re.compile(r".*( \*\*\* disconnected| *<Disconnect>)")
    RE_MATCH_INVALID_UNDEFINED = re.compile(r".*(INVALID|<undefined>).*")
    # Regular expression to split the line into its components -
    # Do a basic sanity test
    #                                    name      date        time      raw
    RE_MATCH_IOC_LOG_LINE = re.compile(r"(\S+)\s+([0-9-]+)\s+([0-9:.]+)\s+(.+)")
    
    RE_MATCH_RAW_DIGITS_WITH_SIGN = re.compile(r"([0-9-]+)")
    RE_MATCH_DIGIT_IN_PARANTHES = re.compile(r"\(([0-9-]+)\)")
    #StatusCode 2021-12-14 16:47:39.780947 IDLE
    RE_MATCH_ALPHA = re.compile(r"([A-Z]+)")
    motion_ongoing = False
    for i in range(0, len(lines)):
        try:
            yvalue = None
            match_raw_DIGITS_WITH_SIGN = None
            line = lines[i]
            line = line.strip()
            match_ioc_pv_not_found = RE_MATCH_IOC_PV_NOT_FOUND.match(line)
            if match_ioc_pv_not_found != None:
                continue
            match_ioc_pv_disconnected = RE_MATCH_IOC_PV_DISCONNECTED.match(line)
            if match_ioc_pv_disconnected != None:
                continue

            match_ioc_log_line = RE_MATCH_IOC_LOG_LINE.match(line)
            if match_ioc_log_line == None:
                print("match_ioc_log_line == None line=%s" % (line))
                sys.exit(1)

            pvname = match_ioc_log_line.group(1)
            date = match_ioc_log_line.group(2)
            time = match_ioc_log_line.group(3)
            raw = match_ioc_log_line.group(4)
            if debug:
                print(
                    "match_ioc_log_line=%s pvname=%s date=%s time=%s raw='%s' "
                    % (match_ioc_log_line, pvname, date, time, raw)
                )
            match_invalid_undefined = RE_MATCH_INVALID_UNDEFINED.match(raw)
            if match_invalid_undefined != None:
                if debug:
                    print(
                        "match_invalid_undefined=%s raw=%s"
                        % (match_invalid_undefined, raw)
                    )
                continue
            match_raw_ALPHA = RE_MATCH_ALPHA.match(raw)
            if match_raw_ALPHA != None:
                alpha = match_raw_ALPHA.group(1)
                if debug:
                    print(
                        "match_raw_ALPHA='%s' alpha='%s'" % (match_raw_ALPHA, alpha)
                    )
                if alpha == "RESET":
                    motion_ongoing = False
                elif alpha == "IDLE":
                    motion_ongoing = False
                elif alpha == "START":
                    motion_ongoing = True
                elif alpha == "BUSY":
                    motion_ongoing = True
                elif alpha == "STOP":
                    motion_ongoing = True
                elif alpha == "ERROR":
                    motion_ongoing = False
                else:
                    print(
                        "Error: match_raw_ALPHA=%s" % (match_raw_ALPHA)
                    )
                    sys.exit(1)
                continue
            if not motion_ongoing:
                continue
            match_raw_DIGITS_WITH_SIGN = RE_MATCH_RAW_DIGITS_WITH_SIGN.match(raw)
            if debug:
                print(
                    "match_raw_DIGITS_WITH_SIGN=%s" % (match_raw_DIGITS_WITH_SIGN)
                )
            steps = None
            if match_raw_DIGITS_WITH_SIGN != None:
                grp_idx = 1
                steps = int(match_raw_DIGITS_WITH_SIGN.group(grp_idx))
                if debug:
                    print("match_raw_DIGITS_WITH_SIGN=%d" % (steps))
            else:
                match_raw_DIGIT_IN_PARANTHES = RE_MATCH_DIGIT_IN_PARANTHES.match(
                    raw
                )
                if debug:
                    print(
                        "match_raw_DIGIT_IN_PARANTHES=%s"
                        % (match_raw_DIGIT_IN_PARANTHES)
                    )

                if match_raw_DIGIT_IN_PARANTHES != None:
                    grp_idx = 1
                    steps = int(match_raw_DIGIT_IN_PARANTHES.group(grp_idx))
                    if debug:
                        print("match_raw_DIGIT_IN_PARANTHES=%d" % (steps))

            #if steps == None:
            #    continue
            if old_steps != None:
                wrap_so_much = 65536
                if old_steps < 15000 and steps >= 15000:
                    wrap_around_offset = wrap_around_offset - wrap_so_much
                elif old_steps > 15000 and steps <= 15000:
                    wrap_around_offset = wrap_around_offset + wrap_so_much
            old_steps = steps
            yvalue = steps + wrap_around_offset
            
            if yvalue is None:
                print("yvalue is None line=%s" % (line))
                print("  raw=%s" % (raw))
                sys.exit(1)

            x_time = datetime.datetime.fromisoformat(date + " " + time)
            # Split off the fraction of a second.
            x_time_sec_res = str(x_time).split(".")[0]
            if debug:
                print("x_time_sec_res=%s oldSeconds=%s" % (x_time_sec_res, oldSeconds))
            x.append(x_time)
            y.append(yvalue)


        except ValueError:
            print("lines[i]=%s" % (lines[i]))
            print("lines[i].split=%s" % (lines[i].split(" ")))
            sys.exit(1)

    fid.close()
    xnp = np.array(x)
    ynp = np.array(y)

    plt.plot(xnp, ynp)
    plt.gca().xaxis.set_major_formatter(mdates.DateFormatter("%Y-%m-%d %H:%M:%S"))
    plt.gca().yaxis.set_major_formatter(mticker.FormatStrFormatter("%d"))

    for txt in plt.gca().xaxis.get_majorticklabels():
        txt.set_rotation(270)

    plt.tight_layout()
    plt.title(output_file_name)
    if output_file_name is not None:
        if debug:
            print("output_file_name=%s" % (output_file_name))
        plt.savefig(output_file_name)
    plt.show()


if __name__ == "__main__":
    sys.exit(main(sys.argv))

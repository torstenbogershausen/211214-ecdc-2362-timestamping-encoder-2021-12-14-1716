#!/bin/sh
FILEWITHOUTEXT=monXX #monRawMtrEnc

FALSE=0
export FALSE

# path for crate007
PATHINHOME=MCAG_setupMotionDemo.191003-base-7.0.5/epics/modules/ethercatmc/test/pythonscripts
#PATHINHOME=MCAG_setupMotionDemo/epics/modules/ethercatmc/test/pythonscripts
if ! test -s $FILEWITHOUTEXT.txt; then
  #IOC for Crate 007
  IPADDR=172.30.244.38
  if ping -c 1 $IPADDR; then
    rsync -v -v torstenbogershausen@$IPADDR:/home/torstenbogershausen/$PATHINHOME/$FILEWITHOUTEXT.txt ./$FILEWITHOUTEXT.txt
  fi
fi

OTHER_PV=StatusCode
for PVNAME in RawMtrVelo-DC RawMtrStep-DC RawEncStep-DC; do
#for PVNAME in RawMtrStep-UTC; do
  INPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.txt
  OUTPUTFILENAME=$FILEWITHOUTEXT.${PVNAME}.png
  if egrep "${PVNAME}|${OTHER_PV}" $FILEWITHOUTEXT.txt >$INPUTFILENAME; then
    python plot.py $INPUTFILENAME $OUTPUTFILENAME
  fi
done
